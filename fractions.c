#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    int num1, denom1, num2, denom2, result_num, result_denom;
    char sign;

    printf("Enter two fractions in x/y (+, -, *, /) z/w format: ");
    scanf("%d/%d %c %d/%d", &num1, &denom1, &sign, &num2, &denom2);

    switch (sign) {
        case '+':
            result_num = num1 * denom2 + num2 * denom1;
            result_denom = denom1 * denom2;
            break;
        case '-':
            result_num = num1 * denom2 - num2 * denom1;
            result_denom = denom1 * denom2;
            break;
        case '*':
            result_num = num1 * num2;
            result_denom = denom1 * denom2;
            break;
        case '/':
            result_num = num1 * denom2;
            result_denom = num2 * denom1;
            break;
        default:
            break;
    }
    //find greatest common denominator
    int d, n;
    
    d = abs(result_denom); 
    n = abs(result_num);

    while(n > 0) {
        int remainder = d % n;
        d = n;
        n = remainder;
    }

    if (d < 1)
        d = 1;

    int reduced_num, reduced_denom;

    reduced_num = result_num / d;
    reduced_denom = result_denom / d;

    // if numerator and denominator are negative then make them both positive
    if (reduced_num < 0 && reduced_denom < 0) {
        reduced_num = abs(reduced_num);
        reduced_denom = abs(reduced_denom);
    }

    if (reduced_denom == 1)
        printf("The result is %d\n", reduced_num);
    else
        printf("The result is %d/%d\n", reduced_num, reduced_denom);

    return 0;
}