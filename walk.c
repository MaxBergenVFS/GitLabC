#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define SIZE 10
#define LEN (sizeof(a) / sizeof(a[0]))

int main(void)
{
    char grid[SIZE][SIZE];
    char a[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};

    //init grid[][] values
    for (int row = 0; row < SIZE; row++) {
        for (int col = 0; col < SIZE; col++)
            grid[row][col] = '.';
    }

    //init a[] starting position
    int pos_row = 0, pos_col = 0;
    grid[pos_row][pos_col] = a[0];


    //seed rand
    srand((unsigned) time(NULL));
    int dir;

    //set remaining a[] positions
    for (int i = 1; i < LEN; i++) {
       
        int temp_row, temp_col, attempts = 0;
        dir = rand() % 4;
        
        get_new_dir:
        if (dir > 3)
            dir = 0;
        if (attempts > 3)
            goto print_grid;

        temp_row = pos_row;
        temp_col = pos_col;
        
        switch (dir) {
            case 0:
                temp_row++;
                break;
            case 1:
                temp_row--;
                break;
            case 2:
                temp_col++;
                break;
            case 3:
                temp_col--;
                break;
            default:
                break;
        }

        //if new position is outside the grid or already occupied, try again
        if (temp_row < 0 || temp_row > SIZE - 1 || temp_col < 0 || temp_col > SIZE - 1 || grid[temp_row][temp_col] != '.') {
            dir++;
            attempts++;
            goto get_new_dir;
        }

        //set new position
        pos_row = temp_row;
        pos_col = temp_col;
        grid[pos_row][pos_col] = a[i];
    }

    print_grid:
    for (int row = 0; row < SIZE; row++) {
        for (int col = 0; col < SIZE; col++)
            printf("%2c ", grid[row][col]);
        printf("\n");
    }
}

