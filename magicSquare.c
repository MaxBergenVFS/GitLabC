#include <stdio.h>
#include <stdbool.h>

int main(void)
{
    int n;

    printf("Enter size of magic square (odd number between 1 and 99): ");
    scanf("%d", &n);

    //init grid
    int a[n][n];
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            a[i][j] = 0;

    //set starting pos
    int col = n / 2;
    int row = 0;

    for (int i = 1; i < n * n + 1; i++) {
        //set new number on grid
        a[row][col] = i;

        //go up one row and over one col
        int temp_row = row - 1;
        int temp_col = col + 1;

        //if new pos is out of bounds then wrap around
        if (temp_row < 0)
            temp_row = n - 1;
        if (temp_col > n - 1)
            temp_col = 0;

        //if new pos is already occupied then reset pos and move down one row
        if (a[temp_row][temp_col]) {
            temp_row = row + 1;
            temp_col = col;
            //wrap if necessary
            if (temp_row > n - 1)
                temp_row = 0;
        }

        //set new pos
        row = temp_row;
        col = temp_col;
    }

    //print grid
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            printf("%3d ", a[i][j]);
        }
        printf("\n");
    }
    
}