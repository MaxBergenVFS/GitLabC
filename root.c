#include <stdio.h>
#include <math.h>

int main(void)
{
    double x, y;
    
    printf("Enter a positive number: ");
    scanf("%lf", &x);

    x = fabs(x);
    y = x / 2;

    for (;;) {
        double oldY = y;
        y = (y + (x / y)) / 2;
        if (fabs(oldY - y) < y * 0.00001) break;
    }

    printf("Square root: %.5lf\n", y);

}